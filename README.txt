Download the repo.
Add the repo folder to system environmental variables.
restart the command prompt if already open.

The commands are ready to use.

Command : gp 
Usage : gp <commit message>
Used to : commit the changes with given message and push to upstream branch

Command : ga
Used to : add all changes (git add)

Command : gc 
Usage : gc <commit message>
Used to : commit added changes with given commit message

Command : gco
Usage : gco [-b] <branch or filename>
Used to : create/checkout to new/ another branch or checkout the file/folder.

Command : gm
Usage : gm <remote name> <branch name>
Used to : merge current branch with new branch

Command : gpl
Usage : gpl <remote name> <branch name>
Used to : (Similar to gm) pull the branch code

Command : gs
Used to : check the current status of changes